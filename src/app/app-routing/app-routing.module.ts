import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { CommonModule } from '@angular/common'

import { LoginComponent } from '../login/login.component'
import { CollectionOperationComponent } from '../collection-operation/collection-operation.component'
import { CallSearchComponent } from '../call-search/call-search.component'
import { CustomerDetailsComponent } from '../customer-details/customer-details.component'
import { CollectionStratergyComponent } from '../collection-stratergy/collection-stratergy.component'

const routes: Routes = [
  {
    path: "",
    component: LoginComponent
  },
  {
    path: "strategy",
    component: CollectionStratergyComponent
  },
  {
    path: "operation",
    component: CollectionOperationComponent
  },
  {
    path: "search",
    component: CallSearchComponent
  },
  {
    path: "customerDetails/:phone_contacted/:CARD_ID",
    component: CustomerDetailsComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      enableTracing: false,
      useHash: true
    }),
    CommonModule
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule {}
