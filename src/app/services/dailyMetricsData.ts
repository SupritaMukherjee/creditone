export class Daily_Metrics_Data_Type {
    metric_type:string;
    name:string;
    value: string;
    RPC: number;
    PTP: number;
    URGENT: number;
    PTP_DIALER_PERCENT: number;
    PTP_CALLER_PERCENT: number;
    PTP_RPC_PERCENT: number;
    CARD_TYPE: string;
}
