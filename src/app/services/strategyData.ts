export class Consistency_Achieved {
    month: string;
    year: string;
    market_share: number;
    cumulative_rfr_average: number;
    RFR: number;
    company: string;
    CARD_TYPE: string;
}
