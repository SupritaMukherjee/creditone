export class customerDataType {
  AGENT_NAME: string;
  LAST_ATTEMPT: string;
  DISPOSITION: string;
  DURATION: number;
}
