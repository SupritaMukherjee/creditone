import 'rxjs/add/operator/map'

import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs'
import { of } from 'rxjs'
import { catchError, map, tap } from 'rxjs/operators'
import { ReturnStatement } from '@angular/compiler'

import { DataType } from './data-type'
import { customerDataType } from './customerData'
import { accountDetailsType } from './accountData'
import { DailySummaryType } from './operationDailyData'
import { RPCbyTimeType } from './rpcByTimeData'
import { Daily_Metrics_Data_Type } from './dailyMetricsData'

const httpOptions = {
  headers: new HttpHeaders({ "Content-Type": "application/json" })
};

@Injectable({
  providedIn: "root"
})
export class HttpServiceService {
  private _url = "http://10.242.76.48:6575";
  private searchDataByPh = this._url + "/search/";
  private customerDataByCardId = this._url + "/calllogs/";
  private accountDataByCardId = this._url + "/accountdetails/";
  private dailyOperationSummary = this._url + "/daily_summary?";
  private dailyMetrics = this._url + "/daily_metrics?";
  private rpcByTime = this._url + "/hourly_credit_risk?";
  private urgentPayment = this._url + '/cycle_del_summary?';
  private dollarCollected = this._url + '/monthly_dollar_collected?';
  private consistencyAchieved = this._url + '/monthly_summary?';
  private apiUrl = '../assets/data/data.json';

 

  private handleError<T>(operation = "operation", result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
  constructor(private http: HttpClient) {}
 
//  Data.json

getLocalData(): Observable<any> {
  return this.http.get<any>(this.apiUrl).pipe(
    tap(dailySummaryDetails => {}),
    catchError(this.handleError("getDetails", []))
  );
}

// Search => search api
  searchDataConfig(searchData: number): Observable<any> {
    return this.http.get<any>(this.searchDataByPh + searchData).pipe(
      tap(searchData => {}),
      catchError(this.handleError("getDetails", []))
    );
  }
  // Customer Data Display
  displayCustomerData(customerData: number): Observable<any> {
    return this.http.get<any>(this.customerDataByCardId + customerData).pipe(
      tap(customerDetails => {}),
      catchError(this.handleError("getDetails", []))
    );
  }
  // Account Data
  displayAccountData(accountData: number): Observable<any> {
    return this.http.get<any>(this.accountDataByCardId + accountData).pipe(
      tap(accountDetails => {}),
      catchError(this.handleError("getDetails", []))
    );
  }
  // Operation Dashboard => Overall Summary:
  dailySummaryData(dateType: number, cardType:string): Observable<any> {
    return this.http.get<any>(this.dailyOperationSummary + "date="+dateType +"&type="+cardType).pipe(
      tap(dailySummaryDetails => {}),
      catchError(this.handleError("getDetails", []))
    );
  }
  // Operation Dashboard => Metrics Table
  dailyMetricsData(dateType: number, cardType: string): Observable<any> {
      return this.http.get<any>(this.dailyMetrics + "date="+dateType +"&type="+cardType).pipe(
        tap(dailyMetricsDetails => {}),
        catchError(this.handleError("getDetails", []))
      );
    
  }
 
  // Credit One Risk Level and RPC by Time:
  risk_level_rpc_by_time(dateType: number, cardType: string): Observable<any> {
    return this.http.get<any>(this.rpcByTime + "date="+dateType +"&type="+cardType).pipe(
      tap(rpcByTimeDetails => {}),
      catchError(this.handleError("getDetails", []))
    );
  }

  // Operation Dashboard => Urgent Payment Collected:
  urgent_payemnt_collected(dateType: number, cardType: string): Observable<any> {
    return this.http.get<any>(this.urgentPayment + "date="+dateType +"&type="+cardType).pipe(
      tap(uregntPaymentDetails => {}),
      catchError(this.handleError("getDetails", []))
    );
  }

  dollarCollectedData(monthValue:number,yearValue:number,cardType: string): Observable<any> {
    return this.http.get<any>(this.dollarCollected + "month="+monthValue+"&year="+yearValue+ "&type=" + cardType).pipe(
      tap(dollarCollectedDataDetails => { }),
      catchError(this.handleError("getDetails", []))
    );
  }

  consistencyAchievedData(monthValue:number,yearValue:number,cardType: string): Observable<any> {
    return this.http.get<any>(this.consistencyAchieved + "month="+monthValue+"&year="+yearValue+ "&type=" + cardType).pipe(
      tap(consistencyAchievedDataDetails => { }),
      catchError(this.handleError("getDetails", []))
    );
  }
}
