import * as Highcharts from 'highcharts'
import * as highchartsHeatmap from 'highcharts/modules/heatmap'
import { Injectable } from '@angular/core'
import { analyzeAndValidateNgModules } from '@angular/compiler';
const HighchartsMore = require("highcharts/highcharts-more.src");
HighchartsMore(Highcharts);
const HC_solid_gauge = require("highcharts/modules/solid-gauge.src");
HC_solid_gauge(Highcharts);


export class DataLabelForHeatMap {
  constructor(public enabled:any, public color:any){

  }
}
@Injectable({
  providedIn: 'root'
})
export class HighchartsService {

  constructor() { }
  barchart(barChartId, xAxis, yAxis,legendName,chartHeight,yAxisText,barColor) {
    Highcharts.chart(barChartId, {
      chart: {
          height:chartHeight,
          type: 'column',
          options3d: {
              enabled: true,
              // alpha: 15,
            beta: 20
            // depth: 70
          }
      },exporting: {
        enabled: false
        },
      title: {
          text: ''
      },
      colors:barColor,
      plotOptions: {
          column: {
              dataLabels:{
                enabled:true
              },
            depth: 25,
            colorByPoint: true,        
          }
      },
      legend: {
        itemStyle: {
            color: '#000000',
            fontWeight: 'bold'
        }
    },
      xAxis: {
        categories: xAxis,
          labels: {
              skew3d: true,
              style: {
                  fontSize: '16px'
              }
          },
      },
      yAxis: {
          title: {
              text: yAxisText
          }
      },
      series: [{
          pointWidth: 40,
          type: 'column',
          name: legendName,
          data: yAxis
         
        }],
        
        credits: {
                enabled: false
              },
  });
}
heatmap(chartId, xAxis , yAxis , plotValue){ 
  Highcharts.chart(chartId,{
    chart: {
      type: "heatmap"
    },
    title: {
      text: " "
    },
    exporting: {
        enabled: false
        },
    colorAxis: {
      min: 0,
      minColor: "#FFFFFF",
      maxColor: "rgb(64, 155, 154)"
    },
    tooltip: {
      formatter: function() {
        return (
          "</b> Time: <b>"  +
          this.series.xAxis.categories[this.point.x] +
          "</b><br> RPC: <b>" +
          this.point.value +
          "</b><br> Risk Level: <b>" +
          this.series.yAxis.categories[this.point.y] +
          "</b>"
        );
      }
    },
    xAxis: {
      categories: xAxis,
      title: {
        text: "Time"
      },
    },
    credits: {
      enabled: false
    },
    yAxis: {
      categories: yAxis,
      title: {
        text: "Risk level"
      },
    },
    legend: {
      align: "right",
      layout: "vertical",
      margin: 25,
      verticalAlign: "top",
      y: 50,
      symbolHeight: 280
    },
    plotOptions: {
      series: {
          dataLabels: {
              enabled: true
          }
      }
  },
    series: [
      {
        pointWidth: 1,
        data: plotValue,
      }
    ]
  });
}

donutChart(chartId,chartData,chartHeight,donutColor){   
  Highcharts.chart(chartId,{
    chart: {
      type: 'pie',
    //   options: {
    //       enabled: true,
    //       alpha: 45
    //   },
      height:chartHeight
  },
  exporting:{
    enabled:false
},
  title:{
    text:''
},
legend:{
    enabled:true
},
  plotOptions: {
      pie: {
          innerSize: 120,
          depth: 45,
          colors:donutColor,
          showInLegend: true,
          dataLabels: {
            enabled: true,
            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
            // style: {
            //     color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
            // }
        }
      }
  },
  credits: {
    enabled: false
},
  series: [{
      data: chartData,
     
  },
]
  });
}

gaugeChart(gaugeChartId,chartData,suffixStr,gaugeColor,rankValue){ 

    Highcharts.chart(gaugeChartId, {
    chart: {
      type: 'solidgauge',
      height:180,
  },
  exporting:{
    enabled:false
},
  title: null,
  pane: {
      center: ['50%', '70%'],
      size: '100%',
      startAngle: -90,
      endAngle: 90,
      background: {
          backgroundColor: '#EEE',
          innerRadius: '60%',
          outerRadius: '100%',
          shape: 'arc',
      },
  },
  tooltip: {
      enabled: false
  },
  // the value axis
  yAxis: {
      stops: [
        gaugeColor
      ],
      lineWidth: 0,
      minorTickInterval: null,
      tickAmount: 2,
      title: {
          y: 60,
          text: suffixStr,
          style:{
            "color": "#636e72", 
            fontWeight: 'bold'
          }
      },
      labels: {
          y: 16,
      },
      min: 0,
      max: 100,
  },
  plotOptions: {
      solidgauge: {
          dataLabels: {
            enabled:true,
              y: 12,
              borderWidth: 0,
              useHTML: true,
              
              format: '<div style="text-align:center;margin-top:40px"><span style="font-size:15px;color:' +('#636e72') + '">{point.y:,.2f}%</span><br/><span>{point.*}</span></div>'
          },
          tooltip: {
            valueSuffix:suffixStr
        }
      },
    },
      credits: {
          enabled: false
      },
      series:[{
        data: [chartData]
    }]
  });
}

columnChart(columChartId,xAxisCategories,chartData,chartHeight,columnColor,yAxisLabel,tooltipData){
    console.log('chartData', chartData)
    
    Highcharts.chart(columChartId,{
        chart: {
            type: 'column',
            height:chartHeight
        },
        title:{
            text:''
        },
        exporting: {
            enabled: false
            },
        xAxis: {
            categories: xAxisCategories
        },
        yAxis: {
            // allowDecimals: false,
            //  tickInterval: 1,
            title:{
                text: yAxisLabel
            },
             min:0,
             max:100
        },
        plotOptions: {
            column: {
                dataLabels: {
                    enabled: true,
                    style: {
                        fontSize: '9px',
                        
                    }
                },
                enableMouseTracking: true
            },
           
        },
        series: chartData,
        tooltip:{
            // formatter:function(){
            //     return 'Rank: <b>' + this.point.y + '</b>'
            // }
            // headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            // pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            //     '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
            // footerFormat: '</table>',
            // shared: true,
            // useHTML: true
        },
        colors:columnColor,
        
        credits: {
            enabled: false
          }
    });
    
}
}
