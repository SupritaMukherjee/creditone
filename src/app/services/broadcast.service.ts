import { Injectable } from '@angular/core'
import { Subject } from 'rxjs'

@Injectable({
  providedIn: "root"
})
export class BroadcastService {
  constructor() {}

  private loggedInAnnouncedSource = new Subject<any>();

  // Observable string streams
  loginAnnounced$ = this.loggedInAnnouncedSource.asObservable();

  confirmLoggedIn(loggedInValue: any) {
    this.loggedInAnnouncedSource.next(loggedInValue);
  }
}
