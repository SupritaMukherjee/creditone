export class DailySummaryType {
  DIALER_DOWNLOAD: number;
  CALL_LOG: number;
  ATTEMPTED: number;
  CONTACTED: number;
  PENETRATION: number;
  IDLE: number;
  date_selected: string;
}
