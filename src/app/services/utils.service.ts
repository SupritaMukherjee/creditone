import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor() { }
   monthNameNumberMapping = {'01':'01','02':'02','03':'03','04':'04','05':'05','06':'06','07':'07','08':'08','09':'09','10':'10','11':'11','12':'12'};
  
    monthNameMapping = {'01':'Jan','02':'Feb','03':'Mar','04':'Apr','05':'May','06':'Jun','07':'Jul','08':'Aug','09':'Sep','10':'Oct','11':'Nov','12':'Dec'};
  
     previousPickMonth = 6;  
  
    prepareMonthArray(selectedMonthVal) {
      let selectedMonthValue = Number(selectedMonthVal);
      var monthArray =  {'1':'01','2':'02','3':'03','4':'04','5':'05','6':'06','7':'07','8':'08','9':'09','10':'10','11':'11','12':'12'};
      var finalMonthArray = [];
      for(var i = selectedMonthValue - this.previousPickMonth+1 ; i < selectedMonthValue+1; i++  ) {
         var tempValue =  monthArray[i];
         finalMonthArray.push(tempValue);
      }
     return finalMonthArray;
  }
  
  prepareMonthYearArray(selectedMonth,selectedYear){
    let monthArray =this.prepareMonthArray(selectedMonth);
    var finalYearArray=[];
    
    for(var i=0;i<monthArray.length;i++){
      let monthName = this.monthNameMapping[monthArray[i]];
      if(selectedYear && selectedYear.length > 2) {
        monthName = monthName + "'" +  selectedYear.substring(2);
      }
  
      finalYearArray.push(monthName);
    }
    return finalYearArray;
  }
  
    createChartSeriesData(chartData:any,selectedMonth:number) {
      var newObj = {};
      for(var i=0; i< chartData.length; i++) {
          var availableObject = [];
          var isPresent = newObj[chartData[i].company]
          if(isPresent) {
              availableObject = newObj[chartData[i].company];
              availableObject.push(chartData[i]);
          } else {
              availableObject.push(chartData[i]);
              newObj[chartData[i].company] = availableObject;
          }
          
      }
      let chartSeriesHelperData = {} as any;
      for(var x in newObj) {
          let chartDataWithMonth = {} as any;
          for(var j=0; j< newObj[x].length; j++) {
              
              if(this.monthNameNumberMapping[newObj[x][j].month]) {
                  chartDataWithMonth[newObj[x][j].month] = newObj[x][j].Cumulative_rfr_average;
              } else  {
                chartDataWithMonth[newObj[x][j].month] = 0.0;
              }
          }
          chartSeriesHelperData[x] = chartDataWithMonth;
      }
      var monthArray = this.prepareMonthArray(selectedMonth);
      var chartSeriesDataArray = [];
      for(var x in chartSeriesHelperData) {
          let chartSeriesObj = {} as any;
          var data = []
          for(var i =0; i < monthArray.length; i++) {
              if(chartSeriesHelperData[x][monthArray[i]]) {
                data.push(chartSeriesHelperData[x][monthArray[i]])
              } else {
                data.push(0.0);
              }
          }
          chartSeriesObj.name = x;
          chartSeriesObj.data = data;
          chartSeriesDataArray.push(chartSeriesObj);
      }
      return chartSeriesDataArray;
  }

CreateRank(data){
  data.sort(function(a, b){return b-a});
return data;
}



}
