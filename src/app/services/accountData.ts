export class accountDetailsType {
  PHONE: string;
  CUSTOMER_NAME: string;
  CARD_ID: number;
  CREDIT_ACCOUNT_ID: number;
  DAYS_DELINQUENT: number;
  BALANCE: number;
  LAST_ATTEMPT: string;
}
