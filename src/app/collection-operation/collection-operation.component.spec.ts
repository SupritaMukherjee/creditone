import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { CollectionOperationComponent } from './collection-operation.component'

describe("CollectionOperationComponent", () => {
  let component: CollectionOperationComponent;
  let fixture: ComponentFixture<CollectionOperationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CollectionOperationComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollectionOperationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
