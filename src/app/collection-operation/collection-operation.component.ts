import {
  AfterViewInit,
  Component,
  OnDestroy,
  OnInit,
  Pipe
} from "@angular/core";
import { Router } from "@angular/router";
import { FormGroup } from "@angular/forms";
import { DatePipe } from "@angular/common";

import { HttpServiceService } from "../services/http-service.service";
import { HighchartsService } from "../services/highcharts.service";

@Pipe({
  name: "dateFormatPipe"
})
@Component({
  selector: "app-collection-operation",
  templateUrl: "./collection-operation.component.html",
  styleUrls: ["./collection-operation.component.css"]
})
export class CollectionOperationComponent
  implements OnInit, AfterViewInit, OnDestroy {
  dailySummary: string[];
  attempted: string;
  call_log: string;
  dailer_download: string;
  idle: string;
  contacted: string;
  penetration: string;
  dateForm: FormGroup;
  date_format: string;
  // set_date: any = { start_time: new Date() };
  set_date: any = { start_time: new Date("09-09-2018") };
  // cardType:any;
  cardType: any = { intial_type: "single" };
  private categoriesData: any = [];
  private seriesData: any = [];
  metrics_name: any = [];
  metricsData: any = [];
  metrics_auto_data: any = [];
  heatMap_X_Categories: any = [];
  heatMap_Y_Categories: any = [];
  heatMapSeries: any = [];
  heatMapSeriesData: any = [];
  private chartId = "";
  private urgentPaymentTrend: any;
  private showLoader: boolean = true;
  private showNoData: boolean = false;
  metrics_auto : any
  private barChartColor = ["#bada55"];

  // Format Date:
  FormatDate(date_value) {
    
    var datePipe = new DatePipe("en-US");
    date_value = datePipe.transform(date_value, "MM-dd-yyyy");
    return date_value;
  }

  // On Submit:
  handlerChange(date, card_type) {
    
    this.date_format = this.FormatDate(date);
    
    this.OverallSummary(this.date_format, card_type);
    this.MetricsData(this.date_format, card_type);
    this.UrgentPayment(this.date_format, card_type);
    this.RPCbyTime(this.date_format, card_type);
  }

  // Daily Summary - Top Panel
  OverallSummary(date, card) {
    this.httpService.dailySummaryData(date, card).subscribe(api_response => {
      if (api_response.status != 404) {
        this.showLoader = false;
        this.dailySummary = api_response as string[];
        this.attempted = api_response[0].ATTEMPTED;
        this.call_log = api_response[0].CALL_LOG;
        this.dailer_download = api_response[0].DIALER_DOWNLOAD;
        this.idle = api_response[0].IDLE;
        this.contacted = api_response[0].CONTACTED;
        this.penetration = api_response[0].PENETRATION;
      }
    });
  }
  // Format String:
  FormatString(str) {
    str = str.replace(/_|-/g, " ");
    return str;
  }
  // Metrics Data - Table
  MetricsData(date, card) {
    this.httpService.dailyMetricsData(date, card).subscribe(api_response => {
      if (api_response.status != 400) {
        this.showLoader = false;
        this.showNoData = false;
        this.metricsData = api_response;
        for(let i=0; i<this.metricsData.length; i++){
          var string = this.metricsData[i].metric
          var substring = "PERCENT"
          if(string.includes(substring)){
          // this.metrics_auto= 
          }

        }
      } else {
        this.metricsData = [];
        this.showNoData = true;
      }
    });
  }

  // RPC by Time
  RPCbyTime(date, card) {
    this.httpService
      .risk_level_rpc_by_time(date, card)
      .subscribe(api_response => {
        if (api_response[0].status != 404) {
          this.showNoData = false;
          this.showLoader = false;
          this.heatMap_X_Categories = [];
          this.heatMap_Y_Categories = [];
          this.heatMapSeries = [];
          this.chartId = "heatMapChartId";
          for (let i = 0; i < api_response.length; i++) {
            if (
              this.heatMap_X_Categories.indexOf(api_response[i].HOUR_WINDOW) ==
              -1
            ) {
              this.heatMap_X_Categories.push(api_response[i].HOUR_WINDOW);
            }
            if (
              this.heatMap_Y_Categories.indexOf(api_response[i].RISK_LEVEL) ==
              -1
            ) {
              this.heatMap_Y_Categories.push(api_response[i].RISK_LEVEL);
            }
          }
          for (let i = 0; i < api_response.length; i++) {
            let xAxisValue = this.heatMap_X_Categories.indexOf(
              api_response[i].HOUR_WINDOW
            );
            let yAxisValue = this.heatMap_Y_Categories.indexOf(
              api_response[i].RISK_LEVEL
            );
            let dataValue = [];
            dataValue.push(xAxisValue);
            dataValue.push(yAxisValue);
            dataValue.push(Number(api_response[i].RPC));
            this.heatMapSeries.push(dataValue);
          }
          if (
            this.chartId &&
            this.categoriesData.length >= 0 &&
            this.seriesData.length >= 0
          ) {
            this._highChartService.heatmap(
              this.chartId,
              this.heatMap_X_Categories,
              this.heatMap_Y_Categories,
              this.heatMapSeries
            );
          } else {
            document.getElementById(this.chartId).innerText = "";
          }
        } else {
          document.getElementById(this.chartId).innerText = " ";
          this.showNoData = true;
        }
      });
  }

  // Urgent payment Collected:
  UrgentPayment(date, card) {
    this.urgentPaymentTrend = "";
    this.httpService
      .urgent_payemnt_collected(date, card)
      .subscribe(api_response => {
        if (api_response.status != 404) {
          this.showNoData = false;
          this.urgentPaymentTrend = api_response;
          this.seriesData = [];
          this.categoriesData = [];
          if (api_response) {
            this.chartId = "barChartId";
            for (let i in this.urgentPaymentTrend) {
              this.categoriesData.push(this.urgentPaymentTrend[i].CYCLE);
              this.seriesData.push(this.urgentPaymentTrend[i].PTP_AMOUNT);
            }

            if (
              this.chartId &&
              this.categoriesData.length >= 0 &&
              this.seriesData.length >= 0
            ) {
             
              this._highChartService.barchart(
                this.chartId,
                this.categoriesData,
                this.seriesData,
                "Collection Bucket",
               "68%",
               "Payment collected",
               this.barChartColor
              );
            } else {
              document.getElementById(this.chartId).innerText = "";
            }
          }
        } else {
          this.showNoData = true;
          document.getElementById(this.chartId).innerText = " ";
        }
      });
  }

  ngOnInit() {
    //  Call all functions on load:
    this.FormatDate(this.set_date.start_time);
    this.OverallSummary("09-09-2018", "single");
    this.MetricsData("09-09-2018", "single");
    this.UrgentPayment("09-09-2018", "single");
    this.RPCbyTime("09-09-2018", "single");
  }
  ngAfterViewInit() {}
  ngOnDestroy() {}

  constructor(
    private _highChartService: HighchartsService,
    private httpService: HttpServiceService,
    private router: Router
  ) {}
}
