import * as Highcharts from 'highcharts'
import { chart } from 'highcharts'
import {UtilsService} from '../services/utils.service'
import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, Pipe } from '@angular/core'
import { ChartModule } from 'angular2-highcharts'
import { Router } from '@angular/router'
import { FormBuilder, FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms'
import { DatePipe } from '@angular/common'
import { NONE_TYPE } from '@angular/compiler/src/output/output_ast'
import * as _moment from 'moment';
import {OwlDateTimeComponent, DateTimeAdapter, OwlDateTimeFormats , OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { MomentDateTimeAdapter, OWL_MOMENT_DATE_TIME_FORMATS } from 'ng-pick-datetime-moment';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MatDatepicker} from '@angular/material/datepicker';

import { HttpServiceService } from '../services/http-service.service'
import { HighchartsService } from '../services/highcharts.service'
import { DailySummaryType } from '../services/operationDailyData'
import { Moment} from 'moment';

const moment =  _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: "app-collection-stratergy",
  templateUrl: "./collection-stratergy.component.html",
  styleUrls: ["./collection-stratergy.component.css"],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    
        {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS}
],
})
@Pipe({
  name: "dateFormatPipe"
})
export class CollectionStratergyComponent
  implements OnInit, AfterViewInit, OnDestroy {
    
  dateForm: FormGroup;
  cardType: any = { intial_type: "single" };
  startView: string = "year";  
  private legendName="";
  private chartId = "";
  private categoriesData: any = [];
  private seriesData: any = [];
  private rollForwardRateData:any;
  private rankValue:any;
  private marketShareArray:any=[];
  private rankIndexValue:any=[];
  private card_type={intial_type:"single"};
  public showNoData = false;
  defaultMonth : string;
  defaultYear : string;
  set_date: any = { start_time: new Date("10-01-2018") };
  private gaugeChartColor:any=[];
  format_month_value: any;
  format_year_value: any;
  rankSutherland:any;
  rankCredit:any;
  rankEnergizer:any;
  rankQor:any;
  dollarRankSutherland:any;
  dollarRankCredit:any;
  dollarRankEnergizer:any;
  dollarRankQor:any;
  private companyId = ["Sutherland","Credit One","iEnergizer","iQOR"];
  private chartColor = ["#E5194E","#000","#0072AE","#B5BBBD"];
  chosenMonthDate: Date;
  newDateValue :Date;
  
  private companyColorMap = [{"Sutherland":"#E5194E"},{"Credit One":"#000"},{"iEnergizer":"#0072AE"},{"iQOR":"#B5BBBD"}]

  // Format Date:
  FormatMonth(date_value) {
    var datePipe = new DatePipe("en-US");
    date_value = datePipe.transform(date_value, "MM-yyyy");
    let month_value=date_value.substring(0,2);
        return month_value;
  }

  FormatYear(date_value){
    var datePipe = new DatePipe("en-US");
    date_value = datePipe.transform(date_value, "MM-yyyy");
    let year_value = date_value.substring(3);
    return year_value;
  } 

  date = new FormControl(moment());
     chosenYearHandler(normalizedYear: Moment) {
      const ctrlValue = this.date.value;     
      ctrlValue.year(normalizedYear.year());
      this.date.setValue(ctrlValue); 
      this.newDateValue= ctrlValue
    }
    chosenMonthHandler(normlizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
      const ctrlValue = this.date.value;
      ctrlValue.month(normlizedMonth.month());
      this.date.setValue(ctrlValue);
      datepicker.close();
    }

  DollarCollected(month,year,card_type){
    this.httpService.dollarCollectedData(month,year,card_type).subscribe(api_response => {
      console.log('api_response', api_response)
      console.log('month,year,card_type', month,year,card_type)
      
     this.categoriesData = [];
     this.seriesData =[];
    
     this.showNoData = false;
      if(api_response[0].status!=404){
        this.legendName = "$ Collected"
        let barChartId = "dollarCollectedID";
        this.showNoData = false;
        for(var i =0;i<api_response.length;i++){
          this.categoriesData.push(api_response[i].company)
          this.seriesData.push(api_response[i].dollar_collected)
          if(api_response[i].company==="Sutherland"){
            this.dollarRankSutherland = api_response[i].RANK;
          }
          if(api_response[i].company==="Credit One"){
            this.dollarRankCredit = api_response[i].RANK;
          }
          if(api_response[i].company==="iEnergizer"){
            this.dollarRankEnergizer = api_response[i].RANK;
          }
          if(api_response[i].company==="iQor"){
            this.dollarRankQor = api_response[i].RANK;
          }
        } 
              
        this._highChartService.barchart(
          barChartId,
          this.categoriesData,
          this.seriesData,
          this.legendName,
          "60%",
          "$(USD)",
          this.chartColor
        );
      }else{
        this.showNoData = true;
       // document.getElementById('dollarCollectedID').innerHTML='';
      }
     })
  }
MarketShare(month,year,card_type){
  this.httpService.consistencyAchievedData(month,year,card_type).subscribe(api_response =>{
    this.showNoData = false;
    if(api_response[0].status!=404){
      
      this.showNoData = false;
      this.marketShareArray=[];
      let marketchartId = "marketShareID";
      for(var i=0;i<api_response.length;i++){
        let tempArray = [];
        tempArray.push(api_response[i].company);
        tempArray.push(api_response[i].market_share);
        this.marketShareArray.push(tempArray);
      }
           this._highChartService.donutChart(
            marketchartId,this.marketShareArray,"60%",this.chartColor
          );
          this.marketShareArray=[];
    }else{
      this.showNoData = true;
      this.marketShareArray=[];
    }
  });
}

  ConsistencyAchieved(month,year,card_type){
    this.httpService.consistencyAchievedData(month,year,card_type).subscribe(api_response =>{
      this.showNoData = false;
      if(api_response[0].status!=404){  
        this.showNoData = false;
        let dataTooltip = api_response;
        let consistencyChartId = "consistencyID";
        let monthYearArray = this._utilService.prepareMonthYearArray(month,year);
        let chartSeriesData = this._utilService.createChartSeriesData(api_response,month)
          this._highChartService.columnChart(
            consistencyChartId,monthYearArray,chartSeriesData,"25%",this.chartColor,'% Consistency Achieved',dataTooltip
        );
      }else{
        this.showNoData = true;
      }
    });
  }

RollForwardData(month,year,card_type){
  this.httpService.consistencyAchievedData(month,year,card_type).subscribe(api_response =>{
    this.showNoData = false;
    if(api_response[0].status!=404){
      this.showNoData = false;
   for(var i=0;i<api_response.length;i++){
    let tempArray = [];
    tempArray.push(api_response[i].company);
    tempArray.push(api_response[i].market_share);
    this.marketShareArray.push(tempArray);

if(api_response[i].company==="Sutherland"){
  this.rankSutherland = api_response[i].RANK;
}
if(api_response[i].company==="Credit One"){
  this.rankCredit = api_response[i].RANK;
}
if(api_response[i].company==="iEnergizer"){
  this.rankEnergizer = api_response[i].RANK;
}
if(api_response[i].company==="iQOR"){
  this.rankQor = api_response[i].RANK;
}
   if(api_response[i].company==this.companyId[i]){
      this.chartId = this.companyId[i];
      this.gaugeChartColor=this.chartColor[i];
      this.rollForwardRateData=((api_response[i].RFR)*100);
      this.rankValue=(api_response[i].RANK)
      
      this.rankIndexValue='Rank:'+ this.rankValue;
      
      this._highChartService.gaugeChart(
        this.chartId,
        this.rollForwardRateData,
        this.companyId[i],
        [this.rollForwardRateData,this.gaugeChartColor],this.rankValue
    );
  }
}}else{
  this.showNoData = true;
}
  });
}

   // On Submit:
  onSubmit(card) {
    if(this.newDateValue === undefined){
    this.format_month_value = this.FormatMonth(this.set_date.start_time);
    this.format_year_value = this.FormatYear(this.set_date.start_time);
    }else{
      this.format_month_value = this.FormatMonth(this.newDateValue);
      this.format_year_value = this.FormatYear(this.newDateValue);
    }
    this.ConsistencyAchieved(this.format_month_value,this.format_year_value,card);
    this.DollarCollected(this.format_month_value,this.format_year_value,card);
    this.MarketShare(this.format_month_value,this.format_year_value,card);
    this.RollForwardData(this.format_month_value,this.format_year_value,card);
   
  }

  ngOnInit() {
    this.defaultMonth=this.FormatMonth(this.set_date.start_time);
    this.defaultYear = this.FormatYear(this.set_date.start_time);
    //  Call all functions on load:   
    this.MarketShare(this.defaultMonth,this.defaultYear,this.card_type.intial_type);
    this.ConsistencyAchieved(this.defaultMonth,this.defaultYear,this.card_type.intial_type);
    this.DollarCollected(this.defaultMonth,this.defaultYear,this.card_type.intial_type);
    this.RollForwardData(this.defaultMonth,this.defaultYear,this.card_type.intial_type);
  }
  ngAfterViewInit() { }
  ngOnDestroy() { }
  constructor(
    private _highChartService: HighchartsService,
    private httpService: HttpServiceService,
    private router: Router,
    private _utilService:UtilsService
  ) {
  }
}
