import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollectionStratergyComponent } from './collection-stratergy.component';

describe('CollectionStratergyComponent', () => {
  let component: CollectionStratergyComponent;
  let fixture: ComponentFixture<CollectionStratergyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollectionStratergyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollectionStratergyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
