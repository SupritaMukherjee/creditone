import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core'
import { Router } from '@angular/router'
import { CookieService } from 'ngx-cookie'

import { BroadcastService } from '../services/broadcast.service'

@Component({
  selector: "app-top-navigation",
  templateUrl: "./top-navigation.component.html",
  styleUrls: ["./top-navigation.component.css"]
})
export class TopNavigationComponent implements OnInit {
  constructor(
    private router: Router,
    private _cookieService: CookieService,
    private broadcastService: BroadcastService
  ) {}

  ngOnInit() {}
  onLogout() {
    this.broadcastService.confirmLoggedIn({ isUserLoggedIn: false });
    this._cookieService.removeAll();
    this.router.navigate(["/"]);
  }
}
