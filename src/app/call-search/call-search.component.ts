import { Component, Input, OnDestroy, OnInit } from '@angular/core'
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser'
import { HttpClient } from '@angular/common/http'
import { HttpErrorResponse } from '@angular/common/http'
import { Data, Router } from '@angular/router'
import { FormsModule, NgModel } from '@angular/forms'
import { FormBuilder, FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms'

import { HttpServiceService } from '../services/http-service.service'
import { DataType } from '../services/data-type'

@Component({
  selector: "app-call-search",
  templateUrl: "./call-search.component.html",
  styleUrls: ["./call-search.component.css"]
})
export class CallSearchComponent implements OnInit {
  url: SafeResourceUrl = "";
  searchNumber: FormGroup;
  display_table: Boolean = false;
  display_noRecords: Boolean = false;
  callSearchData: string[];
  public searchText: number;

  constructor(
    private sanitizer: DomSanitizer,
    private httpService: HttpServiceService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.searchNumber = this.formBuilder.group({
      phone_number: ["", Validators.required]
    });
  }

  ngOnInit() {}
  onKeydown(event) {
    if (event.key === "Enter") {
    }
  }
  onSubmit(model: DataType) {
   
    this.httpService.searchDataConfig(model.phone_number).subscribe(api_response => {
      if(api_response[0].status === 404){
        this.display_table = false;
        this.display_noRecords = true;
      }else{
      this.callSearchData = api_response as string[];
      this.display_table = true;
      this.display_noRecords = false;

      }
    });
  }
  redirect(response: any) {
    this.router.navigate(["customerDetails", response.phone_contacted, response.CARD_ID]);
  }
}
