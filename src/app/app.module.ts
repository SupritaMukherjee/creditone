import * as highcharts from 'highcharts'
import * as highchartsHeatmap from 'highcharts/modules/heatmap'
import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { HttpClientModule } from '@angular/common/http'
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms'
import { BsDropdownModule } from 'ngx-bootstrap/dropdown'
import { TooltipModule } from 'ngx-bootstrap/tooltip'
import { ModalModule } from 'ngx-bootstrap/modal'
import { AngularFontAwesomeModule } from 'angular-font-awesome'
import { CookieModule } from 'ngx-cookie'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { CalendarModule, DateAdapter } from 'angular-calendar'
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns'
import {
  MatDatepickerModule,
  MatFormFieldModule,
  MatInputModule,
  MatNativeDateModule,
  MatSelectModule,
} from '@angular/material'
import { HighchartsChartComponent } from 'highcharts-angular'
import { ChartModule } from 'angular2-highcharts'
import { HighchartsStatic } from 'angular2-highcharts/dist/HighchartsService'

import { GrdFilterPipe } from './services/grd-filter.pipe'
import { AppComponent } from './app.component'
import { LoginComponent } from './login/login.component'
import { AppRoutingModule } from './app-routing/app-routing.module'
import { TopNavigationComponent } from './top-navigation/top-navigation.component'
import { CollectionStratergyComponent } from './collection-stratergy/collection-stratergy.component'
import { CollectionOperationComponent } from './collection-operation/collection-operation.component'
import { CallSearchComponent } from './call-search/call-search.component'
import { HttpServiceService } from './services/http-service.service'
import { CustomerDetailsComponent } from './customer-details/customer-details.component'
import { DatePickerComponent } from './date-picker/date-picker.component';
import { FooterComponent } from './footer/footer.component'

export function highchartsFactory() {
  highchartsHeatmap(highcharts);
  return highcharts;
}
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    TopNavigationComponent,
    CollectionStratergyComponent,
    CollectionOperationComponent,
    CallSearchComponent,
    GrdFilterPipe,
    CustomerDetailsComponent,
    DatePickerComponent,
    HighchartsChartComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    AngularFontAwesomeModule,
    ReactiveFormsModule,
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    CookieModule.forRoot(),
    BrowserAnimationsModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatInputModule,
    MatSelectModule,
    ChartModule.forRoot(
      require('highcharts'),
      require('highcharts/modules/exporting'),
      require('highcharts/highcharts-3d')
    )
  ],
  exports: [BsDropdownModule, TooltipModule, ModalModule],
  providers: [
    HttpServiceService,
    {
      provide: HighchartsStatic,
      useFactory: highchartsFactory
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
