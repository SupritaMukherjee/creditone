import { Component, OnInit } from '@angular/core'
import { FormsModule, NgModel } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'
import { FormBuilder, FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms'
import { first } from 'rxjs/operators'
import { CookieService } from 'ngx-cookie'

import { LoginService } from '../services/login.service'
import { BroadcastService } from '../services/broadcast.service'

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  returnUrl: string;
  loginStatus = false;
  formErrors = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private loginService: LoginService,
    private broadcastService: BroadcastService,
    private _cookieService: CookieService
  ) {}
  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ["", [Validators.required]],
      password: ["", [Validators.required]]
    });
    var cookeyValue = this.getCookie("userLoggineIn");

    if (cookeyValue && cookeyValue === "true") {
      // console.log("cookeyValue in login" + cookeyValue)
      //this.router.navigate(['/dashboard'])
    }
  }

  _keyPress(event: any) {
    const pattern = /^[a-zA-Z_.0-9]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      this.formErrors = true;
      event.preventDefault();
    }
  }
  onSubmit(model: any, valid: boolean) {
    console.log("user name " + model.username);

    this.submitted = true;
    this.formErrors = false;
    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    if (model.username != "analyst" && model.password != "analyst") {
      this.loginStatus = true;
    } else {
      this.loginStatus = false;
    }
    if (model.username == "analyst") {
      var userId = "10";
      this.broadcastService.confirmLoggedIn({
        isUserLoggedIn: true,
        userId: userId
      });
      this._cookieService.put("userLoggineIn", "true");
      this.router.navigate(["/strategy"]);
    }
    //this.loading = true;
    // this.loginService.login(this.f.username.value, this.f.password.value)
    //     .subscribe(
    //         data => {
    //             this.router.navigate(["/dashboard"]);
    //         },
    //         error => {
    //             this.loading = false;
    //         });
  }

  getCookie(key: string) {
    return this._cookieService.get(key);
  }
}
