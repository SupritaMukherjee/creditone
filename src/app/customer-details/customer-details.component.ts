import { Component, Input, OnInit } from '@angular/core'
import { ActivatedRoute, ParamMap } from '@angular/router'
import { HttpClient } from '@angular/common/http'
import { HttpErrorResponse } from '@angular/common/http'
import { Data, Router } from '@angular/router'

import { HttpServiceService } from '../services/http-service.service'

@Component({
  selector: "app-customer-details",
  templateUrl: "./customer-details.component.html",
  styleUrls: ["./customer-details.component.css"]
})
export class CustomerDetailsComponent implements OnInit {
  private customer_phone;
  private customer_card_id;
  customerDetailData: string[];
  accountDetail: string[];
  private contacted: string;
  agent_name: string;
  customer_name: string;
  phone_number: string;
  card_id: string;
  credit_account_id: string;
  days_deliquent: string;
  balance: string;
  last_attempt: string;

  @Input()
  newValue: any;
  constructor(
    private route: ActivatedRoute,
    private httpService: HttpServiceService,
    private router: Router
  ) {}

  ngOnInit() {
    this.customer_phone = this.route.snapshot.paramMap.get("phone_contacted");
    this.customer_card_id = this.route.snapshot.paramMap.get("CARD_ID");

    // Account Details:
    this.httpService
      .displayAccountData(this.customer_card_id)
      .subscribe(value => {
        this.accountDetail = value;
        this.customer_name = value[0].CUSTOMER_NAME;
        this.phone_number = value[0].PHONE;
        this.card_id = value[0].CARD_ID;
        this.credit_account_id = value[0].CREDIT_ACCOUNT_ID;
        this.days_deliquent = value[0].DAYS_DELINQUENT;
        this.balance = value[0].BALANCE;
        this.last_attempt = value[0].LAST_ATTEMPT;
      });

    //  Customer Details: table
    this.httpService
      .displayCustomerData(this.customer_card_id)
      .subscribe(value => {
        this.customerDetailData = value;
        for (let i in value) {
          value[i].DURATION === 0 ? this.contacted = "N": this.contacted = "Y";
          }
      });
  }

  back() {
    this.router.navigate(["search"]);
  }
}
